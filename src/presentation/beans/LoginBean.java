/**
 * @Design Team: Yellow Group
 * @Team Members: Bishop David, Hyde Christopher
 * @Class: CST-235 Computer Programming III
 * @Instructor: Mark Smithers
 *
 * @About: UserBean class
 * @Created: December 2, 2018
 *
 * @Change Log
 *	- 12/2/18 Chris Hyde: Created loginBean to handle user input properties for login.xhtml
**/

package presentation.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ManagedBean(name ="login")
@ViewScoped()
public class LoginBean {

	//Properties
	@NotNull
	@Size(min=4, max=15)
	private String userName;
	
	@NotNull
	@Size(min=5, max=20)
	private String password;
	
	private String logType;
	
	private boolean logged;
	
	public LoginBean() {
		this.userName = "";
		this.password = "";
		this.logType = "Login";
		this.logged = false;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the isLogged
	 */
	public boolean isLogged() {
		return logged;
	}

	/**
	 * @param isLogged the isLogged to set
	 */
	public void setLogged(boolean logged) {
		this.logged = logged;
	}

	/**
	 * @return the logType
	 */
	public String getLogType() {
		return logType;
	}

	/**
	 * @param logType the logType to set
	 */
	public void setLogType(String logType) {
		this.logType = logType;
	}
	
	
}
