/**
 * @Design Team: Yellow Group
 * @Team Members: Bishop David, Hyde Christopher
 * @Class: CST-235 Computer Programming III
 * @Instructor: Mark Smithers
 *
 * @About: UserBean class
 * @Created: November 28, 2018
 *
 * @Change Log
 *	- 11/28/18 Chris Hyde: Created UserBean Class for session registration validation.
**/

package presentation.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@ManagedBean(name ="user")
@ViewScoped()
public class UserBean {

	//User Properties with validation annotation
	
	@NotNull
	@Size(min=3, max=20)
	private String firstName;
	
	@NotNull
	@Size(min=3, max=20)
	private String lastName;
	
	@NotNull
	@Pattern(regexp="(^$|[0-9]{10})")
	private String phoneNumber;
	
	
	@NotNull
	@Size(min=4, max=15)
	private String userName;
	
	@NotNull
	@Size(min=5, max=20)
	private String password;
	
	@NotNull
	@Email
	private String email;
	
	/**
	 * Default Constructor
	 */
	public UserBean() {
		this.email = "";
		this.firstName = "";
		this.lastName = "";
		this.userName = "";
		this.password = "";
		this.phoneNumber = "";
	}

	//--- Getters & Setters ---

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}


	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}


	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}


	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}


	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}


	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}


	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
	
	
	
}
