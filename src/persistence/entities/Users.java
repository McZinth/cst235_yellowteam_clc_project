/**
 * @Design Team: Yellow Group
 * @Team Members: Bishop David, Hyde Christopher
 * @Class: CST-235 Computer Programming III
 * @Instructor: Mark Smithers
 *
 * @About: NavController handle all actions involving links
 * @Last Updated: November 27, 2018
 *
 * @Change Log
 *	- 11/28/28 Chris Hyde: Created Users Class that will handle the Hashmap of users TODO: replace this class in Milestone 4.
**/

package persistence.entities;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.MapKeyColumn;


@Entity
public class Users {

	//Properties
	@ElementCollection(fetch = FetchType.EAGER)
	@MapKeyColumn(name = "User Name")
	private Map<String, User> users = new HashMap<String, User>();

	//--- Getters & Setters ---
	
	/**
	 * @return the users
	 */
	public Map<String, User> getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(Map<String, User> users) {
		this.users = users;
	}
	
	
	

}
