/**
 * @Design Team: Yellow Group
 * @Team Members: Bishop David, Hyde Christopher
 * @Class: CST-235 Computer Programming III
 * @Instructor: Mark Smithers
 *
 * @About: NavController handle all actions involving links
 * @Last Updated: November 27, 2018
 *
 * @Change Log
 *	- 11/28/28 Chris Hyde: Created User entity class for database persistence of registered users.
**/

package persistence.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;


@Entity
public class User {

	//User Properties with validation annotation
		@TableGenerator(name = "user_gen", table = "ID_GEN", pkColumnName = "GEN_NAME", valueColumnName = "GEN_VAL", allocationSize = 1)
		@Id
		@GeneratedValue(strategy = GenerationType.TABLE, generator = "user_gen")
		private long userID;
		
		@Column(name = "First Name")
		private String firstName;
		
		@Column(name = "Last Name")
		private String lastName;
		
		@Column(name = "Email")
		private String email;
		
		@Column(name = "Phone #")
		private String phoneNumber;
		
		@Column(name = "User Name")
		private String userName;
		
		@Column(name = "Password")
		private String password;

		/**
		 * default constructor (Required by entity annotation)
		 */
		public User() {
		}
		
		
		/** Constructor with params
		 * @param userName
		 * @param password
		 * @param firstName
		 * @param lastName
		 * @param email
		 * @param phoneNumber
		 */
		public User(String userName, String password, String firstName, String lastName, String email,
				String phoneNumber) {
			super();
			this.userName = userName;
			this.password = password;
			this.firstName = firstName;
			this.lastName = lastName;
			this.email = email;
			this.phoneNumber = phoneNumber;
		}

		//--- Getters & Setters ---

		/**
		 * @return the userID
		 */
		public long getUserID() {
			return userID;
		}

		/**
		 * @param userID the userID to set
		 */
		public void setUserID(long userID) {
			this.userID = userID;
		}

		/**
		 * @return the firstName
		 */
		public String getFirstName() {
			return firstName;
		}

		/**
		 * @param firstName the firstName to set
		 */
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		/**
		 * @return the lastName
		 */
		public String getLastName() {
			return lastName;
		}

		/**
		 * @param lastName the lastName to set
		 */
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		/**
		 * @return the email
		 */
		public String getEmail() {
			return email;
		}

		/**
		 * @param email the email to set
		 */
		public void setEmail(String email) {
			this.email = email;
		}

		/**
		 * @return the phoneNumber
		 */
		public String getPhoneNumber() {
			return phoneNumber;
		}

		/**
		 * @param phoneNumber the phoneNumber to set
		 */
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}

		/**
		 * @return the userName
		 */
		public String getUserName() {
			return userName;
		}

		/**
		 * @param userName the userName to set
		 */
		public void setUserName(String userName) {
			this.userName = userName;
		}

		/**
		 * @return the password
		 */
		public String getPassword() {
			return password;
		}

		/**
		 * @param password the password to set
		 */
		public void setPassword(String password) {
			this.password = password;
		}
		
		
		
		
	
}
