/**
 * @Design Team: Yellow Group
 * @Team Members: Bishop David, Hyde Christopher
 * @Class: CST-235 Computer Programming III
 * @Instructor: Mark Smithers
 *
 * @About: NavController handle all actions involving links
 * @Last Updated: November 27, 2018
 *
 * @Change Log
 *	- 11/27/28 Chris Hyde: Created class for handle all page/view change request.
**/

package logic.controllers;

import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import persistence.entities.User;
import persistence.entities.Users;
import presentation.beans.LoginBean;
import presentation.beans.UserBean;

@ManagedBean(name = "navCtrl")
@SessionScoped()
public class NavController {

	Users users = new Users();
	Map<String, User> userMap = users.getUsers();

	// --- Change Views ---

	/**
	 * Handle navigating to login view Todo: Inject Logic bean to handle if already
	 * logged in
	 */
	public String viewLogin(LoginBean login) {
		if(login.isLogged()) {
			login.setLogType("Login");
			return viewHome();
		}else {
			return "login.xhtml";
		}
	}

	/**
	 * Handles navigating back to home view.
	 */
	public String viewHome() {
		return "index.xhtml";
	}

	/**
	 * Handles navigating to register view TODO: Inject logic bean to handle if
	 * already logged in
	 */
	public String viewRegister() {
		return "register.xhtml";
	}

	// --- Button actions ---
	public String onRegister(UserBean user) {
		// Request userBean session map
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("userBean", user);

		// Create a new entity user using UserBean property data
		User u = new User(user.getUserName(), user.getPassword(), user.getFirstName(), user.getLastName(),
				user.getEmail(), user.getPhoneNumber());

		// Add user entity to users hashmap<User>
		users.getUsers().put(user.getUserName(), u);

		for (String s : users.getUsers().keySet()) {
			System.out.println(s);
		}
		// Go to login page
		return "login.xhtml";

	}

	public String onLogin(LoginBean login) {
		// Request loginBean session map
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("login", login);

		// Search hashmap for userName

		if (users.getUsers().containsKey(login.getUserName())) {
			// set a temp user entity to match the entity in hashmap
			User tempUser = users.getUsers().get(login.getUserName());
			// check temp user if password matches
			if (tempUser.getPassword().equals(login.getPassword())) {
				// set logged to true
				login.setLogged(true);
				login.setLogType("Logout");
				// go to home page
				return viewHome();
			} else {
				System.out.println("No Password Match Found");
			}

		} else {
			System.out.println("No User Name Match Found");
		}

		return viewRegister();

	}

}
